ActiveAdmin.register Portofolio do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :client, :portofolio_desc, :year, portofolio_images_attributes: [:id, :name, :name_cache, :category, :_destroy] 
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
config.clear_sidebar_sections!

form(:html => { :multipart => true }) do |f|
  f.semantic_errors *f.object.errors.keys
  f.inputs "Portofolio" do
    f.input :title
    f.input :client
    f.input :year
    f.input :portofolio_desc, as: :text
      f.has_many :portofolio_images do |ff|
        ff.input :name, :as => :file, input_html: {class: 'file'}, :hint => ff.template.image_tag(ff.object.name.url(:thumb))
        ff.input :name_cache, :as => :hidden 
        ff.input :category, as: :select, collection:["thumbnail","gallery"]
      end
    f.actions
  end
end

show title: :title do
  attributes_table do
  row :client
  row :year  
  row 'Description' do |r|
    r.portofolio_desc
  end
  row "Images"  do
     ul do
      portofolio.portofolio_images.each do |img|
        li do 
          image_tag(img.name.url)
        end
      end
     end
  end
  end
end

index do
  selectable_column
  column :title
  column :client
  column :year
  actions
end

end
