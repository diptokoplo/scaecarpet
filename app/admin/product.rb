ActiveAdmin.register Product do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :product_desc, product_images_attributes: [:id, :name, :name_cache, :category, :_destroy]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
config.clear_sidebar_sections!

form(:html => { :multipart => true }) do |f|
  f.semantic_errors *f.object.errors.keys
  f.inputs "Product" do
    f.input :name
    f.input :product_desc, as: :text
      f.has_many :product_images do |ff|
        ff.input :name, :as => :file, input_html: {class: 'file'}, :hint => ff.template.image_tag(ff.object.name.url(:thumb))
        ff.input :name_cache, :as => :hidden 
        ff.input :category, as: :select, collection:["thumbnail","gallery"]
      end
    f.actions
  end
end

show title: :name do
  attributes_table do
  row 'Description' do |r|
    r.product_desc
  end
  row "Images"  do
     ul do
      product.product_images.each do |img|
        li do 
          image_tag(img.name.url)
        end
      end
     end
  end
  end
end

index do
  selectable_column
  column :name
  actions
end

end
