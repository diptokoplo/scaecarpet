ActiveAdmin.register SliderImage do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

config.clear_sidebar_sections!

form do |f|
  f.semantic_errors *f.object.errors.keys
  f.inputs "Slider Images" do
        f.input :name, :as => :file
        f.input :name_cache, :as => :hidden 
    f.actions
  end
end

show do
  attributes_table do
  row "Slider Images"  do
     ul do
          image_tag(slider_image.name.url)
      end
     end
  end
end
index do
  selectable_column
  column :name
  actions
end
end
