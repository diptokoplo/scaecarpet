class SliderImagesController < ApplicationController
  def show
    @slider_image = SliderImage.find(params[:id])
  end
  def index
    @slider_images = SliderImage.all
  end
end
