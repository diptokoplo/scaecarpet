class StaticPagesController < ApplicationController
  def home
    @slider_images = SliderImage.all
    @products = Product.all
    @portofolios = Portofolio.all
  end

  def about_us
  end

  def contact
  end

end
