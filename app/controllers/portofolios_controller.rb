class PortofoliosController < ApplicationController
  def show
    @portofolio = Portofolio.find(params[:id])
    @portofolio_images = @portofolio.portofolio_images.where(category: "gallery")
    @portofolio_thumbnail = @portofolio.portofolio_images.where(category: "thumbnail")
  end

  def index
    @portofolios = Portofolio.all
    @portofolios = Kaminari.paginate_array(@portofolios).page(params[:page]).per(3)
  end
end
