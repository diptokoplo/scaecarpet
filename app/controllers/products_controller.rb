class ProductsController < ApplicationController

  def show
    @product = Product.find(params[:id])
    @product_images = @product.product_images.where(category: "gallery")
    @product_thumbnail = @product.product_images.where(category: "thumbnail")
  end

  def index
    @products = Product.all
  end



end
