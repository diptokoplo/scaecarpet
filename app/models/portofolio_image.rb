class PortofolioImage < ApplicationRecord
  belongs_to :portofolio
  validates :category, :presence => {:message => '=> You must choose category!'}
  mount_uploader :name, ImageUploader
  
end
