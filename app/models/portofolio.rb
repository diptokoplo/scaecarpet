class Portofolio < ApplicationRecord
  has_many :portofolio_images
  validates :portofolio_images, :presence => {:message => "=> You have to upload at least one image for each category!"}
  accepts_nested_attributes_for :portofolio_images, allow_destroy: true
  paginates_per 3
end
