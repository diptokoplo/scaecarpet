class Product < ApplicationRecord
  has_many :product_images
  validates :product_images, :presence => {:message => "=> You have to upload at least one image for each category!"}
  accepts_nested_attributes_for :product_images, allow_destroy: true
end
