class ProductImage < ApplicationRecord
  belongs_to :product
  validates :category, :presence => {:message => '=> You must choose category!'}
  mount_uploader :name, ImageUploader
end
