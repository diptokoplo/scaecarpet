Rails.application.routes.draw do

  root 'static_pages#home'

  get 'about' => 'static_pages#about_us'

  get 'contact' => 'static_pages#contact'
  
  resources :products , only: [:show, :index], path: 'products'
  resources :portofolios , only: [:show, :index], path: 'portofolios'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
