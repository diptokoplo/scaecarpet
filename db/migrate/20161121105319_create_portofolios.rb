class CreatePortofolios < ActiveRecord::Migration[5.0]
  def change
    create_table :portofolios do |t|
      t.string :title
      t.string :client
      t.integer :year
      t.string :portofolio_desc
      t.timestamps null: false
    end
  end
end
