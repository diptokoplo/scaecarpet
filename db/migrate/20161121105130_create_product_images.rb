class CreateProductImages < ActiveRecord::Migration[5.0]
  def change
    create_table :product_images do |t|
      t.string :name
      t.string :category
      t.integer :product_id
      t.references :project, index: true
      t.timestamps null:false
    end
  end
end
