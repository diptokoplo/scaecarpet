class CreatePortofolioImages < ActiveRecord::Migration[5.0]
  def change
    create_table :portofolio_images do |t|
      t.string :name
      t.string :category
      t.integer :portofolio_id
      t.references :portofolio, index: true
      t.timestamps null:false
    end
  end
end
